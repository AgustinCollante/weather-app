import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';

//Components
import {ActivityIndicator, Modal, View, StyleSheet} from 'react-native';

const LoaderModal = () => {
  const loader = useSelector(store => store.app.loader);

  useEffect(() => {}, [loader]);

  return (
    <Modal transparent visible={loader}>
      <View style={styles.modal}>
        <ActivityIndicator color={'lightblue'} size={42} />
      </View>
    </Modal>
  );
};

export default LoaderModal;

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
