import React, {useRef} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';

//Components
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

//Styles
import Icon from 'react-native-vector-icons/Ionicons';

const AutoComplete = ({addCity}) => {
  const mapRef = useRef();

  const handleCleanInput = () => {
    mapRef.current.setAddressText('');
  };

  return (
    <ScrollView
      contentContainerStyle={{
        flexDirection: 'row',
        paddingHorizontal: 15,
        paddingVertical: 10,
      }}>
      <GooglePlacesAutocomplete
        ref={mapRef}
        onFail={err => console.log(err)}
        keepResultsAfterBlur={true}
        placeholder="Ingresa una ubicación"
        styles={{
          container: {
            backgroundColor: 'white',
            ...styles.shadow,
            borderRadius: 7,
            paddingHorizontal: 5,
          },
          textInput: {},
          poweredContainer: {display: 'none'},
        }}
        keyboardShouldPersistTaps={'handled'}
        listUnderlayColor={'transparent'}
        textInputProps={{
          numberOfLines: 1,
        }}
        minLength={3}
        returnKeyType={'search'}
        listViewDisplayed={false}
        fetchDetails={false}
        renderDescription={row => row.description}
        onPress={(data, details = null) => {
          let city = data?.description?.split(',')[0];
          addCity(city.toLowerCase());
          handleCleanInput();
        }}
        filterReverseGeocodingByTypes={[
          'locality',
          'administrative_area_level_3',
        ]}
        nearbyPlacesAPI="GoogleReverseGeocoding"
        getDefaultValue={() => ''}
        query={{
          key: 'AIzaSyBbkwsWYSIALb7dqsaX8QuG1T1t6fSM5Hc',
          language: 'es', // language of the results
          types: '(cities)', // default: 'geocode'
        }}
        debounce={200}
        renderRightButton={() => (
          <View style={{justifyContent: 'center', width: 30}}>
            <Icon name="search" size={24} color={'grey'} />
          </View>
        )}
      />
    </ScrollView>
  );
};

export default AutoComplete;

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center',
    backgroundColor: 'white',
    width: '100%',
    height: 30,
    borderRadius: 7,
  },
  shadow: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.19,
    shadowRadius: 5.62,
    elevation: 6,
  },
});
