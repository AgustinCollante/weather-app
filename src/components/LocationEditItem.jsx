import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

//Styles
import FontAwesome from 'react-native-vector-icons/FontAwesome';

//Utils
import {firstLetterCapitalize} from '../utils/functions';

const LocationEditItem = ({city, deleteCity}) => {
  return (
    <View style={styles.editItem}>
      <View style={{alignItems: 'center'}}>
        <Text style={{color: 'white', fontSize: 22}}>
          {city && firstLetterCapitalize(city)}
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => deleteCity(city)}
        style={styles.deleteButton}>
        <FontAwesome name="trash-o" color="white" size={24} />
      </TouchableOpacity>
    </View>
  );
};

export default LocationEditItem;

const styles = StyleSheet.create({
  editItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgba(160, 160, 160, 0.7)',
    width: '90%',
    padding: 15,
    marginBottom: 15,
    borderRadius: 15,
  },
  deleteButton: {
    backgroundColor: 'red',
    height: 45,
    width: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
});
