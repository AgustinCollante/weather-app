import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

//Utils
import {formatDate, weatherType} from '../utils/functions';

const TempDay = ({tempDay}) => {
  return (
    <View style={styles.tempDay}>
      <View style={{alignItems: 'center'}}>
        <Text style={{...styles.text, marginHorizontal: 5}} testID='dateTempDay'>
          {tempDay && formatDate(tempDay.date)}
        </Text>
        <Image
          testID='imgTempDay'
          source={{
            uri: `https:${tempDay?.day?.condition?.icon}`,
          }}
          style={styles.icon}
        />
        <Text style={styles.text} testID="weatherTempDay">
          {tempDay && weatherType(tempDay?.day?.condition?.text)}
        </Text>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={styles.tempLabel} testID="minMaxTempDay">
          {Math.floor(tempDay?.day?.maxtemp_c)}°C | {Math.floor(tempDay?.day?.mintemp_c)}°C
        </Text>
      </View>
    </View>
  );
};

export default TempDay;

const styles = StyleSheet.create({
  tempDay: {
    marginHorizontal: 10,
    marginVertical: 20,
    padding: 15,
    backgroundColor: 'rgba(160, 160, 160, 0.5)',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderRadius: 20,
  },
  text: {
    color: 'white',
    fontSize: 18,
  },
  tempLabel: {
    color: 'white',
    fontSize: 24,
  },
  icon: {
    width: 40,
    height: 40,
  },
});
