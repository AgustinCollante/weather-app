import React from 'react';
import { View, Text, ScrollView } from 'react-native';

//Hooks
import { useDispatch, useSelector } from 'react-redux';

//Actions
import { setSelectedCity } from '../redux/weather';

//Components
import TempCity from './TempCity';

const OtherCities = ({cities}) => {
  const dispatch = useDispatch();
  const currentPosition = useSelector(store => store.weather.currentPosition);
  const selectedCity = useSelector(store => store.weather.selectedCity);

  const handleSelectCity = (city) => {
    dispatch(setSelectedCity(city));
  };


  return (
    <View style={{paddingHorizontal: 10}}>
        <Text style={{color: 'white', fontSize: 24, textDecorationLine:'underline'}}>Tus ciudades</Text>
        <ScrollView horizontal showsHorizontalScrollIndicator={false} >
          <TempCity currentPosition tempCity={currentPosition} selectCity={handleSelectCity} selectedCity={selectedCity}  />
          {cities && cities.map( (e, i) => <TempCity key={i} tempCity={e} selectCity={handleSelectCity} selectedCity={selectedCity}/>)}
        </ScrollView>
    </View>
  )
}

export default OtherCities;