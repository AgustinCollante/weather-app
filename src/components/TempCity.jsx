import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';

//Utils
import {weatherType} from '../utils/functions';

const TempCity = ({currentPosition, tempCity, selectCity, selectedCity}) => {

  const showBorder = () => {
    if (!currentPosition && selectedCity === tempCity?.location?.name) return true;
    if (currentPosition && !selectedCity) return true;
  };

  return (
    <TouchableOpacity
      style={{
        ...styles.tempDay,
        borderColor: 'lightblue',
        borderWidth: showBorder() ? 2 : 0,
      }}
      onPress={() =>
        selectCity(currentPosition ? '' : tempCity?.location?.name)
      }>
      <View style={{alignItems: 'center'}}>
        <Text style={{...styles.text, marginHorizontal: 5}}>
          {tempCity?.location?.name}
        </Text>
        <Image
          source={{
            uri: `https:${tempCity?.current?.condition?.icon}`,
          }}
          style={styles.icon}
        />
        <Text style={styles.text}>
          {tempCity && weatherType(tempCity?.current?.condition?.text)}
        </Text>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={styles.tempLabel}>
          {Math.floor(tempCity?.current?.temp_c)}°c{' '}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default TempCity;

const styles = StyleSheet.create({
  tempDay: {
    marginHorizontal: 10,
    marginVertical: 20,
    padding: 15,
    backgroundColor: 'rgba(160, 160, 160, 0.5)',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderRadius: 20,
  },
  text: {
    color: 'white',
    fontSize: 18,
  },
  tempLabel: {
    color: 'white',
    fontSize: 24,
  },
  icon: {
    width: 40,
    height: 40,
  },
});
