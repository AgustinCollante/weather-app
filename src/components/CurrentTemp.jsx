import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

//Utils
import {weatherType} from '../utils/functions';

const CurrentTemp = ({temp}) => {
  return (
    <View style={styles.currentTemp} testID='currentTemp'>
      <View style={{alignItems: 'center'}}>
        <Text style={{fontSize: 64, color: 'white'}}>
          {Math.floor(temp?.temp_c)}°C
        </Text>
        <Text style={{fontSize: 24, color: 'white'}}>
          {weatherType(temp?.condition?.text)}
        </Text>
      </View>
    </View>
  );
};

export default CurrentTemp;

const styles = StyleSheet.create({
  currentTemp:{
    backgroundColor: 'rgba(160, 160, 160, 0.5)',
    width: 200,
    height: 200,
    alignSelf: 'center',
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  }
});
