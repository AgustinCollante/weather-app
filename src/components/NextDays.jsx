import React from 'react';
import { View, Text, ScrollView } from 'react-native';

//Components
import TempDay from './TempDay';

const NextDays = ({forecastTemp}) => {
  let weatherArray = forecastTemp?.slice(1)
  return (
      <ScrollView contentContainerStyle={{paddingHorizontal: 10}} showsHorizontalScrollIndicator={false} horizontal >
        {weatherArray?.length > 0 && weatherArray.map( (e, i) => <TempDay testID='tempDays' key={i} tempDay = {e} />)}
    </ScrollView>

  )
}

export default NextDays;