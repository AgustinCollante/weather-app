import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

//Hooks
import {useNavigation} from '@react-navigation/native';

//Styles
import Icon from 'react-native-vector-icons/Ionicons';

const Header = ({location}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.header}>
      <View style={{opacity: 1}}>
        <TouchableOpacity onPress={() => navigation.navigate('AddCity')} testID="addCityButton">
          <Icon name="ios-add" size={32} color="white" />
        </TouchableOpacity>
      </View>
      <View >
        <Text style={{fontSize: 24, color: 'white', width: location?.name.length > 15 ? '90%' :  '100%' }} numberOfLines={1} ellipsizeMode={'tail'}>{location?.name}</Text>
      </View>
      <View />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    display:'flex',
    flexDirection: 'row',
    backgroundColor: 'rgba(160, 160, 160, 0.5)',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    marginTop: 10,
    marginHorizontal: 5,
    borderRadius: 20,
  },
});
