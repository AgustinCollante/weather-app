import axios from "axios";
import { apiUrl, apiKey } from "../utils/constants";

//config
let baseCurrent = 'current.json'
let baseForecast = 'forecast.json'

export const getCurrentWeather = async (lat, long) => {
  let { data } = await axios.get(`${apiUrl}/${baseForecast}?key=${apiKey}&q=${lat},${long}&days=6&aqi=no`);
  return data;
};
export const getSelectedCityWeather = async (city) => {
   let {data} = await axios.get(`${apiUrl}/${baseForecast}?key=${apiKey}&q=${city}&days=6&aqi=no`)
   return data;
};
export const getCityWeather = (city) => `${apiUrl}/${baseCurrent}?key=${apiKey}&q=${city}&aqi=no`;