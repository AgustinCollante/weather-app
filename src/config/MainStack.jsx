import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//Screens
import Home from '../screens/Home';
import AddCity from '../screens/AddCity';

//Components
import LoaderModal from '../components/Modal';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown:false}}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="AddCity" component={AddCity}/>
        </Stack.Navigator>
        <LoaderModal/>
    </NavigationContainer>
  )
}

export default MainStack;