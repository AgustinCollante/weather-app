import React from 'react';
import {ImageBackground, StyleSheet} from 'react-native';

//Hooks
import useFetchWeather from '../customHooks/useWeather';

//Components
import Header from '../components/Header';
import CurrentTemp from '../components/CurrentTemp';
import NextDays from '../components/NextDays';
import OtherCities from '../components/OtherCities';

//Utils
import {weatherTypeBg} from '../utils/functions';

const Home = () => {
  const allWeather = useFetchWeather();
  const currentLocation = allWeather?.data?.currentLocation?.current?.condition;

  return (
    <ImageBackground
      style={styles.home}
      resizeMethod="resize"
      source={weatherTypeBg(currentLocation?.text)}>
      <Header location={allWeather?.data?.currentLocation.location} />
      <CurrentTemp temp={allWeather?.data?.currentLocation.current} />
      <NextDays
        forecastTemp={allWeather?.data?.currentLocation.forecast.forecastday}
      />
      <OtherCities
        currentCity={allWeather.data?.currentLocation}
        cities={allWeather?.data?.cities}
      />
    </ImageBackground>
  );
};

export default Home;

const styles = StyleSheet.create({
  home: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
});
