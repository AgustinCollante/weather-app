import React from 'react';
import { ScrollView, View, Text, TouchableOpacity} from 'react-native';

//Hooks
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

//Actions
import { setNewCity, deleteCity } from '../redux/weather';

//Components
import LocationEditItem from '../components/LocationEditItem';
import AutoComplete from '../components/Autocomplete';

//Styles
import Icon from 'react-native-vector-icons/Ionicons';


const AddCity = () => {
  const dispatch = useDispatch();
  const cities = useSelector(store => store.weather.cities);
  const navigation = useNavigation();

  const handleAddCity = (city) => {
    dispatch(setNewCity(city));
  };

  const handleDeleteCity = (city) => {
    dispatch(deleteCity(city));
  }

  return (
    <ScrollView>
      <TouchableOpacity style={{marginTop: 20, marginLeft: 5}} onPress={() => navigation.goBack()}>
        <Icon name='ios-arrow-back-outline' color='black' size={32}/>
      </TouchableOpacity>
      <View style={{marginVertical: 15}}>
        <View style={{width: '100%', alignItems:'center'}}>
          <Text style={{fontSize:24}}>Administrar ciudades</Text>
        </View>
        <AutoComplete addCity={handleAddCity}/>
      </View>
      <View style={{alignItems:'center'}} testID='citiesAddCity'>
        {cities.length > 0 && cities.map((e, i) => <LocationEditItem key={i} city={e} deleteCity={handleDeleteCity}/>)}
      </View>
    </ScrollView>
  )
}

export default AddCity;