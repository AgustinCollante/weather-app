import { sunny, cloudy, rainy, storm } from '../assets/images/index';
import moment from "moment";
import 'moment/locale/es';
moment.locale('es-ES');

export const weatherType = (type) => {
    let typeLower = type?.toLowerCase();
    if( typeLower?.includes('rain') && typeLower?.includes('thunder')) return 'Tormenta';
    if( typeLower?.includes('rain')) return 'Lluvia';
    if( typeLower === 'cloudy' || typeLower === 'partly cloudy' || typeLower === 'mist' || typeLower === 'overcast') return 'Nublado';
    return 'Despejado';
}

export const weatherTypeBg = (type) => {
    let typeLower = type?.toLowerCase();
    if( typeLower?.includes('rain') || typeLower?.includes('thunder')) return storm;
    if( typeLower?.includes('rain')) return rainy;
    if( typeLower === 'cloudy' || typeLower === 'partly cloudy' || typeLower === 'mist' || typeLower === 'overcast') return cloudy;
    return sunny;
}

export const formatDate = (date) => {
    let formattedDate = moment(date).locale('es-ES').format('dddd');
    let intialLetter = formattedDate.slice(0,1).toLocaleUpperCase();
    formattedDate = intialLetter + formattedDate.slice(1);
    return formattedDate;
}

export const firstLetterCapitalize = (string) => {
    let intialLetter = string.slice(0,1).toLocaleUpperCase();
    let stringFormatted = intialLetter + string.slice(1);
    return stringFormatted;
}