export const sunny = require('./sunny.jpg');
export const cloudy = require('./cloudy.jpg');
export const rainy = require('./rainy.jpg');
export const storm = require('./storm.jpg');