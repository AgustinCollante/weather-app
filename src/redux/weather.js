// --- CONSTANTES ---
const SET_SELECTED_CITY = 'SET_SELECTED_CITY';
const SET_NEW_CITY = 'SET_NEW_CITY';
const DELETE_CITY = 'DELETE_CITY';
const SET_CURRENT_POSITION = 'SET_CURRENT_POSITION';

// --- STATE ---

const initialState = {
  selectedCity: '',
  currentPosition: {},
  cities:[],
};

// --- REDUCER ---
export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case SET_SELECTED_CITY:
      return {
        ...state,
        selectedCity: action.payload
      }
    case SET_NEW_CITY:
      return {
        ...state,
        cities: [...state.cities, action.payload]
      }
    case DELETE_CITY:
      return {
        ...state,
        cities: action.payload
      }
    case SET_CURRENT_POSITION:
      return {
        ...state,
        currentPosition: action.payload
      }
    default:
      return {
        ...state,
      };
  }
}

// --- ACTIONS ---

export const setNewCity = (city) => async (dispatch, getState) => {
  let cities = getState().weather.cities;
  let index = cities?.findIndex((e) => e === city );
  if(index === -1){
    dispatch({
      type: SET_NEW_CITY,
      payload: city,
     })
  }
}

export const deleteCity = (city) => async (dispatch, getState) => {
  let cities = getState().weather.cities;
  cities = cities.filter((e) => e != city );
  dispatch({
      type: DELETE_CITY,
      payload: cities,
  })
}

export const setSelectedCity = (city) => async (dispatch, getState) => {
  dispatch({
      type: SET_SELECTED_CITY,
      payload: city,
  })
}

export const setCurrentPosition = (city) => async (dispatch, getState) => {
  dispatch({
      type: SET_CURRENT_POSITION,
      payload: city,
  })
}