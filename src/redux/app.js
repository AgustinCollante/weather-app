// --- CONSTANTES ---
const SET_LOADER = 'SET_LOADER';

// --- STATE ---

const initialState = {
  loader: false,
};

// --- REDUCER ---
export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case SET_LOADER:
      return {
        ...state,
        loader: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
}

// --- ACTIONS ---

export const setLoader = state => async (dispatch, getState) => {
  dispatch({
    type: SET_LOADER,
    payload: state,
  });
};
