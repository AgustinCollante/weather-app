import { useEffect, useState } from "react";

//Hooks
import { useDispatch, useSelector } from "react-redux";

//Actions
import { setCurrentPosition } from "../redux/weather";
import { setLoader} from '../redux/app';

//Utils
import axios from "axios";
import { getCurrentWeather, getCityWeather, getSelectedCityWeather } from "../provider/weather";
import { apiUrl, apiKey } from "../utils/constants";
import GetLocation from 'react-native-get-location'

const useFetchWeather = () => {
    const [data, setData] = useState(undefined);
    const [error, setError] = useState(undefined);
    const {cities, selectedCity} = useSelector(store => store.weather);

    const dispatch = useDispatch();

    const fetchAll = async (urls) => {
        const res = await Promise.all(urls.map(u => axios.get(u)))
        const data = await Promise.all(res.map(r => r.data))
        return data;
    }
    
    useEffect(() => {
        dispatch(setLoader(true));
        ( async () => {
            try {
                let location = await GetLocation.getCurrentPosition({ enableHighAccuracy: true, timeout: 15000, });
                let response = {};
                if(!selectedCity){
                    response = await getCurrentWeather(location.latitude, location.longitude);
                    dispatch(setCurrentPosition(response))
                } else {
                    response = await getSelectedCityWeather(selectedCity);
                }
                if(!cities) return
                let urls = cities.map( e => {
                    return getCityWeather(e)
                })
                let allResponses = await fetchAll(urls);
                setData({cities: allResponses, currentLocation: response});
                dispatch(setLoader(false));
            } catch (error) {
                setError(error);
                dispatch(setLoader(false));
            }
        })()
    },[cities, selectedCity]);


    return {data, error }
}

export default useFetchWeather;