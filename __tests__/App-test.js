import React from 'react';
import App from '../App'
import CurrentTemp from '../src/components/CurrentTemp';
import TempDay from '../src/components/TempDay';
import Header from '../src/components/Header';
import AddCity from '../src/screens/AddCity';
import {fireEvent, render, waitFor} from '@testing-library/react-native';
import {currentTempMock, forecastTemp} from '../src/utils/testData';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import * as redux from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import NextDays from '../src/components/NextDays';


const mockedDispatch = jest.fn();
// Mocks like this need to be configured at the top level 
// of the test file, they can't be setup inside your tests.
jest.mock("@react-navigation/native", () => {
  const actualNav = jest.requireActual("@react-navigation/native");
  return {
    ...actualNav,
    useNavigation: () => ({
      navigate: jest.fn(),
      dispatch: mockedDispatch,
    }),
  };
});

let MockTouchable = jest.mock(
  'react-native/Libraries/Components/Touchable/TouchableOpacity.js',
  () => {
    const { TouchableHighlight } = require('react-native')
    const MockTouchable = props => {
      return <TouchableHighlight {...props} />
    }
    MockTouchable.displayName = 'TouchableOpacity'

    return MockTouchable
  }
)

describe(' Home tests ', () => {

  beforeEach(() => {
    mockedDispatch.mockClear();
  });

  test('should render currentTemp box without data', () => {
    let {container} = render(<CurrentTemp />);
    expect(container).toBeDefined();
  });

  test('on first render should not render next days ', () => { 
    let { queryAllByTestId } = render(<NextDays/>)
    expect(queryAllByTestId('tempDays').length).toBe(0)
   })

  test('should render text with correct temp and weather', () => {
    let {getByText} = render(<CurrentTemp temp={currentTempMock} />);
    expect(getByText('19°C')).toBeDefined();
    expect(getByText('Despejado')).toBeDefined();
  });

  test('should render nextDayTemp with day, icon, weather, maxTemp and minTemp', () => {
    let {getByTestId} = render(<TempDay tempDay={forecastTemp[0]} />);
    expect(getByTestId('dateTempDay')).toBeDefined();
    expect(getByTestId('imgTempDay')).toBeDefined();
    expect(getByTestId('weatherTempDay')).toBeDefined();
    expect(getByTestId('minMaxTempDay')).toBeDefined();
  });
});

describe("Add City Test", () => {

  const spy = jest.spyOn(redux, 'useSelector')
  spy.mockReturnValue({ cities:[] })
  const initialState = {cities:[]}
  const mockStore = configureStore()
  let store

  test('should render without cities', () => { 
    store = mockStore(initialState)
    let { getByTestId } = render(<Provider store={store}><NavigationContainer><AddCity/></NavigationContainer></Provider>)
    expect(getByTestId('citiesAddCity').children.length).toBe(0);
   })
})