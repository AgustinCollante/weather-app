import React from 'react';
import MainStack from './src/config/MainStack';

import { Provider } from 'react-redux';
import generateStore from './src/redux/store';

const store = generateStore();

const App = () => {
  return (
    <Provider store={store}>
      <MainStack/>
    </Provider>
  );
};

export default App;